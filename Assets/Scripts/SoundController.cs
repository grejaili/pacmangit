﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundController : MonoBehaviour
{


    public AudioSource introSong;
    public AudioSource wakaSound1;
    public AudioSource wakaSound2;
    public AudioSource deathSound;
    public AudioSource panicSound;
    public AudioSource eatenSound;
    public AudioSource returnSound;
    public AudioSource newLifeSound;
    public AudioSource hurryUpSound;

    public static AudioSource CalledWakaSound1;
    public static AudioSource CalledWakaSound2;
    public static AudioSource CalledDeathSound;

    public static AudioSource CalledEatenSound;
    bool playWaka = true;


    public float wakatimer;
    // Use this for initialization
    void Start()
    {

      
        //panicSound.enabled = false;
        //CalledEatenSound = eatenSound;
        //returnSound.enabled = false;


    }

    // Update is called once per frame

    public void StartGame()
    {

        StartCoroutine(WakaWakaSound());
    }

    IEnumerator WakaWakaSound()
    {
        while(playWaka == true)
        {
            wakaSound1.Play();
            yield return new WaitForSeconds(wakatimer);
            wakaSound2.Play();
        }

    }

    public void DeathSound()
    {
        playWaka = false;
        deathSound.Play();
    }


    void Update()
    {

    }
}
