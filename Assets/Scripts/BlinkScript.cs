﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BlinkScript : MonoBehaviour {

	// Use this for initialization
	Text text;
	void Start () {
		text = GetComponent<Text>();
		StartCoroutine (FlashLabel ());
	}

	IEnumerator FlashLabel()
	{
		while (true) 
		{
			text.enabled = true;
			yield return new WaitForSeconds(0.5f);
			text.enabled = false;
			yield return new WaitForSeconds(0.5f);
		}
	}

	// Update is called once per frame
	void Update () {
	
	}
}
