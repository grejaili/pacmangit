﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class Screens : MonoBehaviour
{
    [SerializeField] Text MiddleText;


    public void EnableMiddleText(string message, float disableTimer)
    {
        MiddleText.enabled = true;
        MiddleText.text = message;

        Invoke("DisableText", disableTimer);

    }


    void DisableText()
    {
        MiddleText.enabled = false;
    }






}
