﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InfoUI : MonoBehaviour
{

    [SerializeField] Data_Scritable data;
    [SerializeField] Text score;
    [SerializeField] Text lives;
    [SerializeField] Text level;



    // Use this for initialization
    void Start()
    {
        data.GameStart();
        UpdateUI();
    }

    public void AddScore(int points)
    {
        data.score += points;
        UpdateUI();
    }

    public void UpdateUI()
    {

        score.text = data.score.ToString();
        lives.text = data.lives.ToString();
        level.text = data.level.ToString();
    }



}
