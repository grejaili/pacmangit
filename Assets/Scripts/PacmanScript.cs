﻿using UnityEngine;
using System.Collections;
using System;

public class PacmanScript : MonoBehaviour
{

    Animator anim;
    private bool updatePacman = false;
    #region Animation
    private int pacmanUpAnimation = Animator.StringToHash("PacmanUpAnimation");
    private int pacmanDownAnimation = Animator.StringToHash("PacmanDownAnimation");
    private int pacmanRightAnimation = Animator.StringToHash("PacmanRightAnimation");
    private int pacmanLeftAnimation = Animator.StringToHash("PacmanLeftAnimation");
    private int pacmanDeathAnimation = Animator.StringToHash("PacmanDeathAnimation");
    #endregion
    public float speed;
    Rigidbody2D rb;
    [HideInInspector] public InfoUI infoUI;
    public GameManager gameManager;
    Collider2D col;
    //[Tooltip("set in inspector")]
    bool alive = true;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();
        col = GetComponent<BoxCollider2D>();
        infoUI = GameObject.FindGameObjectWithTag("UI").GetComponent<InfoUI>();
        gameManager = GameObject.FindGameObjectWithTag("GameController").GetComponent<GameManager>();
        anim.enabled = false;
    }

    void StartUp()
    {
        anim.enabled = true;
        rb.velocity = new Vector2(-speed, 0);
    }

    void Update()
    {
        if(alive)
        {
            Inputs();
        }

    }

    void Inputs()
    {
        if(anim.enabled == false)
        {
            anim.enabled = true;
        }
        if(Input.GetKey(KeyCode.UpArrow))
        {
            transform.position = new Vector2(CalculateNewXPosition(transform.position.x), transform.position.y);
            anim.Play(pacmanUpAnimation);
            rb.velocity = new Vector2(0, speed);
        }
        if(Input.GetKey(KeyCode.DownArrow))
        {
            transform.position = new Vector2(CalculateNewXPosition(transform.position.x), transform.position.y);

            anim.Play(pacmanDownAnimation);
            rb.velocity = new Vector2(0, -speed);
        }
        if(Input.GetKey(KeyCode.RightArrow))
        {
            transform.position = new Vector2(transform.position.x, CalculateNewYPosition(transform.position.y));
            anim.Play(pacmanRightAnimation);
            rb.velocity = new Vector2(speed, 0);
        }
        if(Input.GetKey(KeyCode.LeftArrow))
        {
            transform.position = new Vector2(transform.position.x, CalculateNewYPosition(transform.position.y));
            anim.Play(pacmanLeftAnimation);
            rb.velocity = new Vector2(-speed, 0);
        }
    }




    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.CompareTag("Ghost"))
        {
            //GhostScript ghostScript = coll.GetComponent<GhostScript>();
            //if(ghostScript.enemyStates.Equals(enemyStates.attacking))
            //{

            //    Debug.Log("teste");
            //    Dead();
            //}
            //if(ghostScript.enemyStates.Equals(enemyStates.panic))
            //{
            //    //EAT GHOST
            //}
        }

        //if(coll.CompareTag("Pellet"))
        //{
        //    gameManager.CheckNewLifeScore();
        //}

    }
    public void Dead()
    {
        col.enabled = false;
        rb.velocity = Vector2.zero;
        anim.Play(pacmanDeathAnimation);
        alive = false;
        gameManager.Death();
        Destroy(this.gameObject, gameManager.resetTimer);

    }





    bool valid(Vector2 direction)
    {
        Vector2 pacmanPosition = transform.position;

        RaycastHit2D hit = Physics2D.Linecast(pacmanPosition + direction, pacmanPosition);

        if(hit.collider.isTrigger)
            return true;


        return
            (hit.collider == GetComponent<Collider2D>());
    }

    private float CalculateNewXPosition(float xTransform)
    {
        int mazeIndex;
        float xFloat = (9.45f + xTransform) / 0.7f;
        mazeIndex = (int)System.Math.Round(xFloat, 0);
        float realXPosition = (-9.45f) + (mazeIndex * 0.7f);
        return realXPosition;
    }

    private float CalculateNewYPosition(float yTransform)
    {
        int mazeIndex;
        float yFloat = (10.5f - yTransform) / 0.7f;
        mazeIndex = (int)System.Math.Round(yFloat, 0);
        float realXPosition = 10.5f - (mazeIndex * 0.7f);
        return realXPosition;
    }
}
