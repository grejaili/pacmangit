﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Data", order = 1)]
public class Data_Scritable : ScriptableObject
{

    public int level;
    public int lives;
    public int score;
    public int ScoreUntilNewlife;
    [HideInInspector] public int scoreNewlifeActual;



    [HideInInspector] public float panicTime;
    public float panicTimeLimit; //4 
    public int eatGhostScore; //200
    public int numberOfPilletsLeft = 0;
    public int numberOfPelletsEaten;

    //public int scoreUntilNewLife;


    public void GameStart()
    {
        level = 1;
        lives = 3;
        score = 0;
    }


    public void RemoveLife()
    {
        lives--;
    }

    public void PelletEaten()
    {
        numberOfPilletsLeft--;
        numberOfPelletsEaten++;
    }



}
