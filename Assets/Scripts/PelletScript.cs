﻿using UnityEngine;
using System.Collections;
using System;

public class PelletScript : MonoBehaviour
{

    // Use this for initialization

    [SerializeField] Data_Scritable scritable;
    [SerializeField] int points;
    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.CompareTag("Pacman"))
        {
            PacmanScript pacmanScript = coll.GetComponent<PacmanScript>();

      
            //if(MainGameScript.WakaSoundTurn == 1)
            //{
            //	//MainGameScript.CalledWakaSound1.Play ();
            //	//MainGameScript.WakaSoundTurn = 2;
            //}
            //else
            //{
            //	//MainGameScript.CalledWakaSound2.Play ();
            //	//MainGameScript.WakaSoundTurn = 1;
            //}
            if(tag == "Big Pellete")
            {
                if(pacmanScript.gameManager.CheckGameFinished())
                {
                    GameObject[] allGhosts = GameObject.FindGameObjectsWithTag("Ghost");
                    foreach(GameObject aGhost in allGhosts)
                    {
                        GhostScript gScript = aGhost.GetComponent<GhostScript>();
                        Animator anim = aGhost.GetComponent<Animator>();
                        anim.SetBool("PanicTimeRunningOut", false);
                 

                        if((gScript.enemyState < enemyStates.four || gScript.enemyState > enemyStates.six))
                        {
                            gScript.panicMode = true;
                        }
                        else
                        {
                            gScript.panicMode = false;
                        }
                    }


                    if(pacmanScript.gameManager.gameScript.status == 0)
                    {
                        pacmanScript.gameManager.gameScript.status = 1;
                        pacmanScript.gameManager.gameScript.CallPanicTimer();
                    }
                    else if(pacmanScript.gameManager.gameScript.status == 1)
                    {
                        pacmanScript.gameManager.gameScript.RecallPanicTimer();
                    }
                }
            }

            pacmanScript.infoUI.AddScore(points);

            Destroy(gameObject);
        }

    }


    public void BeatenGame(Collider coll)
    {
        GameObject[] fruits = GameObject.FindGameObjectsWithTag("Fruit");
        if(fruits.Length > 0)
        {
            Destroy(fruits[0]);
        }
        GameObject[] ghosts = GameObject.FindGameObjectsWithTag("Ghost");

        foreach(GameObject aGhost in ghosts)
        {
            GhostScript gScript = aGhost.GetComponent<GhostScript>();
            Animator gAnim = aGhost.GetComponent<Animator>();
            gAnim.enabled = false;
            //  gScript.status = 10;
            aGhost.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }

        coll.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        //  MainGameScript mScript = mainGame.GetComponent<MainGameScript>();
        //mScript.status = 3;
        //mScript.GetComponent<AudioSource>().enabled = false;
        //mScript.panicSound.enabled = false;
        //mScript.returnSound.enabled = false;
        //mScript.hurryUpSound.enabled = false;
        // mScript.StopAllModes();
        MainGameScript.ghostsReturning = false;
        Animator anim = coll.GetComponent<Animator>();
        anim.enabled = false;
        this.GetComponent<Renderer>().enabled = false;
        //mScript.EndGame();
    }
}
