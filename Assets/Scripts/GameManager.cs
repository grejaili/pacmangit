﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public Data_Scritable data;
    public InfoUI infoUI;
    public Screens screens;
    public SoundController soundController;
    public MainGameScript gameScript;
    public float resetTimer;
    public GameObject[] ghosts;



    void Start()
    {
        StartGame();
    }

    public void StartGame()
    {
        soundController.StartGame();
        StartCoroutine(gameScript.LoadGame(resetTimer));
        Invoke(" FindReferences", resetTimer);
    }

    void FindReferences()
    {
        GameObject[] ghosts = GameObject.FindGameObjectsWithTag("Ghost");
    }




    public void Death()
    {
        soundController.DeathSound();
        if(data.lives > 0)
        {
            data.RemoveLife();
            infoUI.UpdateUI();
            StartCoroutine(gameScript.LoadGame(resetTimer));
        }


    }


    public bool CheckGameFinished()
    {
        if(data.numberOfPilletsLeft > 0)
        {
            return true;
        }
        else
            return false;
    }

    public void CreateFruit()
    {
        if(data.numberOfPelletsEaten == 70 || data.numberOfPelletsEaten == 170)
        {

            GameObject fruits = GameObject.FindGameObjectWithTag("Fruit");
            if(fruits != null && CheckGameFinished())
            {
                Instantiate(gameScript.fruit);
            }
        }
    }


    public void CheckNewLifeScore()
    {     
        if(data.score >= data.scoreNewlifeActual)
        {
            //newLifeSound.Play();
            data.scoreNewlifeActual -= data.scoreNewlifeActual;
            data.lives++;
        }
    }

}
