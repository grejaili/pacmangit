﻿using UnityEngine;
using System.Collections;
using System;

public class FruitScript : MonoBehaviour {

	// Use this for initialization
	public Sprite pointsSprite;
	private bool eaten;
	void Start()
	{
		StartCoroutine (WaitUntilDisappearance ());
	}

	void OnTriggerEnter2D(Collider2D coll) {
		if (coll.tag == "Pacman") 
		{
			if (!eaten) {
				eaten = true;
				StopAllCoroutines();
				StartCoroutine(EatenProcedures());
			}
		}
	}

	IEnumerator WaitUntilDisappearance()
	{
		yield return new WaitForSeconds (10f);
		Destroy (this.gameObject);
	}

	IEnumerator EatenProcedures()
	{
		//GetComponent<AudioSource>().Play ();
		//GameObject mainGame = GameObject.Find("MainGame");
		//MainGameScript mainScript = mainGame.GetComponent<MainGameScript>();
		//this.GetComponent<SpriteRenderer>().sprite = mainScript.CurrentFruitPointSprite;
		//GameObject 
        
     // ValueGameObject = GameObject.Find("ScoreValues");
		//GUIText scoreValueGuiText = scoreValueGameObject.GetComponent<GUIText>();
		//int scoreValue = Int32.Parse(scoreValueGuiText.text);
		//scoreValue += mainScript.CurrentFruitPoint;
		//mainScript.CheckNewLifeScore (mainScript.CurrentFruitPoint);
		//scoreValueGuiText.text = scoreValue + "";
		yield return new WaitForSeconds (1.5f);
		Destroy (this.gameObject);
	}
}
