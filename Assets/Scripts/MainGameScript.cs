﻿using UnityEngine;
using System.Collections;
using System;

public class MainGameScript : MonoBehaviour
{


    public Data_Scritable data;
    public GameObject fruit;
    public int status;
    private int quarterOfTotalPellets;
    private int[] fruitPoints = { 100, 300, 500, 700, 1000, 2000, 3000, 5000 };
    public Sprite[] fruitPointsSprites = new Sprite[8];
    private int currentFruitPoint;
    private Sprite currentFruitPointSprite;


    private int[,] pilletLocations
    = new int[,]{
//		 1  2  3  4  5  6  7  8  9  10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28
		{0 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //1
		{0 , 3, 1, 1, 1, 1, 3, 1, 1, 1, 1, 1, 3, 0, 0, 3, 1, 1, 1, 1, 1, 3, 1, 1, 1, 1, 3, 0}, //2
		{0 , 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0}, //3
		{0 , 2, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 2, 0}, //4
		{0 , 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0}, //5
		{0 , 3, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 3, 0}, //6
		{0 , 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0}, //7
		{0 , 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0}, //8
		{0 , 3, 1, 1, 1, 1, 3, 0, 0, 5, 0, 0, 5, 0, 0, 5, 0, 0, 5, 0, 0, 3, 1, 1, 1, 1, 3, 0}, //9
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //10
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //11
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 5, 0, 0, 5, 0, 0, 5, 0, 0, 5, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //12
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //13
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //14
		{5 , 0, 0, 0, 0, 0, 3, 0, 0, 5, 0, 0, 0, 0, 0, 0, 0, 0, 5, 0, 0, 3, 0, 0, 0, 0, 0, 5}, //15
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //16
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //17
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 5, 0, 0, 5, 0, 0, 5, 0, 0, 5, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //18
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //19
		{0 , 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 0}, //20
		{0 , 3, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 3, 0, 0, 3, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 3, 0}, //21
		{0 , 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0}, //22
		{0 , 1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0}, //23
		{0 , 4, 1, 3, 0, 0, 3, 1, 1, 3, 1, 1, 3, 0, 0, 3, 1, 1, 3, 1, 1, 3, 0, 0, 3, 1, 4, 0}, //24
		{0 , 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0}, //25
		{0 , 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 1, 0, 0, 0}, //26
		{0 , 3, 1, 3, 1, 1, 3, 0, 0, 3, 1, 1, 3, 0, 0, 3, 1, 1, 3, 0, 0, 3, 1, 1, 3, 1, 3, 0}, //27
		{0 , 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0}, //28
		{0 , 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0, 0, 1, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 1, 0}, //29
		{0 , 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 1, 1, 3, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 3, 0}, //30
		{0 , 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0}, //31
	};

    private int[,] junctions = new int[31, 28];
    public GameObject smallPellet;
    public GameObject bigPellet;
    public GameObject pacman;
    public GameObject blinky;
    public GameObject pinky;
    public GameObject inky;
    public GameObject clyde;

    public static bool ghostsReturning;
    public Sprite CurrentFruitPointSprite
    {
        get
        {
            return currentFruitPointSprite;
        }
    }

    void Start()
    {
        GhostScript.Junctions = junctions;
        generateMap();
    }

    void generateMap()
    {
        for(int y = 0; y < pilletLocations.GetLength(0); y++)
        {
            for(int x = 0; x < pilletLocations.GetLength(1); x++)
            {
                if(pilletLocations[y, x] == 1)
                {
                    Instantiate(smallPellet, new Vector2(CalculateNewXPosition(x), CalculateNewYPosition(y)), smallPellet.transform.rotation);
                    data.numberOfPilletsLeft++;
                }
                else if(pilletLocations[y, x] == 2)
                {
                    Instantiate(bigPellet, new Vector2(CalculateNewXPosition(x), CalculateNewYPosition(y)), bigPellet.transform.rotation);
                    data.numberOfPilletsLeft++;
                }
                else if(pilletLocations[y, x] == 3)
                {
                    junctions[y, x] = 1;
                    Instantiate(smallPellet, new Vector2(CalculateNewXPosition(x), CalculateNewYPosition(y)), smallPellet.transform.rotation);
                    data.numberOfPilletsLeft++;
                }
                else if(pilletLocations[y, x] == 4)
                {
                    junctions[y, x] = 1;
                    Instantiate(bigPellet, new Vector2(CalculateNewXPosition(x), CalculateNewYPosition(y)), bigPellet.transform.rotation);
                    data.numberOfPilletsLeft++;
                }
                else if(pilletLocations[y, x] == 5)
                {
                    junctions[y, x] = 1;
                }
            }

        }
        quarterOfTotalPellets = data.numberOfPilletsLeft / 4;
    }

    private void IntializeFruitPoints()
    {
        if(data.level <= 7)
        {
            currentFruitPoint = fruitPoints[data.level - 1];
            currentFruitPointSprite = fruitPointsSprites[data.level - 1];
        }
        else
        {
            currentFruitPoint = fruitPoints[fruitPoints.Length - 1];
            currentFruitPointSprite = fruitPointsSprites[fruitPoints.Length - 1];
        }
    }





    //public void EndGame()
    //{
    //    StartCoroutine(WinGame());
    //}

    //IEnumerator WinGame()
    //{
    //    GameObject scoreValueGameObject = GameObject.Find("ScoreValues");
    //    GUIText scoreValueGuiText = scoreValueGameObject.GetComponent<GUIText>();
    //    int scoreValue = Int32.Parse(scoreValueGuiText.text);
    //    PlayerPrefs.SetInt("Score", scoreValue);
    //    PlayerPrefs.SetInt("Lives", lives);
    //    PlayerPrefs.SetInt("Level", level + 1);
    //    PlayerPrefs.SetInt("ScoreUntilNextLife", scoreUntilNewLife);
    //    Instantiate(youWinTitle, new Vector2(gameOverTitle.transform.position.x, gameOverTitle.transform.position.y), gameOverTitle.transform.rotation);
    //    yield return new WaitForSeconds(2f);
    //    Application.LoadLevel(1);
    //}

    public IEnumerator LoadGame(float resetTimer)
    {
        yield return new WaitForSeconds(resetTimer);
        Instantiate(pacman, new Vector2(pacman.transform.position.x, pacman.transform.position.y), pacman.transform.rotation);
        Instantiate(blinky, new Vector2(blinky.transform.position.x, blinky.transform.position.y), blinky.transform.rotation);
        Instantiate(pinky, new Vector2(pinky.transform.position.x, pinky.transform.position.y), pinky.transform.rotation);
        Instantiate(inky, new Vector2(inky.transform.position.x, inky.transform.position.y), inky.transform.rotation);
        Instantiate(clyde, new Vector2(clyde.transform.position.x, clyde.transform.position.y), clyde.transform.rotation);
        IntializeFruitPoints();
        yield return new WaitForSeconds(resetTimer);
    }

    // Update is called once per frame
    void Update()
    {


        bool continuePanicMode = false;
        GameObject[] allGhosts = GameObject.FindGameObjectsWithTag("Ghost");
        foreach(GameObject aGhost in allGhosts)
        {
            GhostScript gScript = aGhost.GetComponent<GhostScript>();
            if(gScript.panicMode)
            {
                continuePanicMode = true;
                break;
            }
        }

        if(!continuePanicMode)
        {
            StopCoroutine(BeginPanicModeTimer());
            GameObject[] allGhosts2 = GameObject.FindGameObjectsWithTag("Ghost");
            foreach(GameObject aGhost in allGhosts2)
            {
                Animator anim = aGhost.GetComponent<Animator>();
                anim.SetBool("PanicTimeRunningOut", false);
            }
            status = 0;
        }

    }

    public void CallPanicTimer()
    {
        data.eatGhostScore = 200;
        StartCoroutine(BeginPanicModeTimer());
    }
    public void RecallPanicTimer()
    {
        data.eatGhostScore = 200;
        StopAllCoroutines();
        StartCoroutine(BeginPanicModeTimer());
    }



    public bool CheckGameFinished()
    {
        if(data.numberOfPilletsLeft <= 0)
        {
            return true;
        }
        else
            return false;
    }

    public void CreateFruit()
    {
        if(data.numberOfPelletsEaten == 70 || data.numberOfPelletsEaten == 170)
        {

            GameObject fruits = GameObject.FindGameObjectWithTag("Fruit");
            if(fruits != null && CheckGameFinished())
            {
                Instantiate(fruit);
            }
        }
    }

    IEnumerator BeginPanicModeTimer()
    {
        float normalPanicTime = data.panicTime * 0.6f;
        float endPanicTime = data.panicTime * 0.4f;
        yield return new WaitForSeconds(normalPanicTime);
        GameObject[] allGhosts = GameObject.FindGameObjectsWithTag("Ghost");
        foreach(GameObject aGhost in allGhosts)
        {
            Animator anim = aGhost.GetComponent<Animator>();
            anim.SetBool("PanicTimeRunningOut", true);
        }
        yield return new WaitForSeconds(endPanicTime);
        foreach(GameObject aGhost in allGhosts)
        {
            Animator anim = aGhost.GetComponent<Animator>();
            anim.SetBool("PanicTimeRunningOut", false);
            GhostScript gScript = aGhost.GetComponent<GhostScript>();
            gScript.panicMode = false;
        }

    }

    private float CalculateNewXPosition(int x)
    {
        float realXPosition = (-9.45f) + (x * 0.7f);
        return realXPosition;
    }


    private float CalculateNewYPosition(int y)
    {
        float realXPosition = 10.5f - (y * 0.7f);
        return realXPosition;
    }

}



