﻿using UnityEngine;
using System.Collections;
using System;

public class GhostScript : MonoBehaviour
{

    public float normalSpeed;
    private GameObject pacman;
    private const float getOutSpeed = 1f;
    System.Random random = new System.Random();
    private static int[,] junctions;
    private Vector2 nextDestination;
    private Vector2 afterExitDestination;
    private Vector2 previousDestination;
    public bool panicMode = false;
    public float returnSpeed;
    public float runAwaySpeed;
    public GameObject corner;
    private GameObject objectToConsider;
    Animator anim;
    bool updateGhost = false;

    public int count = 6;
    public Sprite point200;
    public Sprite point400;
    public Sprite point800;
    public Sprite point1600;

    public enemyStates enemyState;
    public static int[,] Junctions
    {
        get
        {
            return junctions;
        }

        set
        {
            junctions = value;
        }
    }


    // Use this for initialization
    void Start()
    {
        anim = GetComponent<Animator>();
        anim.enabled = false;
        pacman = GameObject.FindGameObjectsWithTag("Pacman")[0];
        afterExitDestination = new Vector2(0, 2.8f);
        if(enemyState == enemyStates.PinkyStart)
        {
            previousDestination = new Vector2(0, 0.7f);
            nextDestination = new Vector2(CalculateNewXPosition(afterExitDestination.x), CalculateNewYPosition(afterExitDestination.y));
        }
        else if(enemyState == enemyStates.ClydeInkyStart)
        {
            previousDestination = new Vector2(0, 0.7f);
            nextDestination = new Vector2(CalculateNewXPosition(afterExitDestination.x), CalculateNewYPosition(afterExitDestination.y));
        }
        else
        {
            previousDestination = new Vector2(0, 2.8f);
            nextDestination = new Vector2(CalculateNewXPosition(transform.position.x), CalculateNewYPosition(transform.position.y));
        }
        StartCoroutine(StartUp());

    }

    IEnumerator ScatterMode()
    {
        objectToConsider = corner;
        yield return new WaitForSeconds(4f);
        StartCoroutine(ChaseMode());
    }

    IEnumerator ChaseMode()
    {
        objectToConsider = pacman;
        yield return new WaitForSeconds(20f);
        count--;
        if(count > 0)
        {
            StartCoroutine(ScatterMode());
        }
    }

    IEnumerator StartUp()
    {
        yield return new WaitForSeconds(2f);
        updateGhost = true;
        anim.enabled = true;
        anim.SetBool("Panic", false);
        anim.SetBool("Return", false);
        if(enemyState == enemyStates.PinkyStart)
        {
            anim.SetInteger("XAxis", 0);
            anim.SetInteger("YAxis", 1);
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, getOutSpeed);
        }
        else if(enemyState == enemyStates.ClydeInkyStart)
        {
            anim.SetInteger("XAxis", 0);
            anim.SetInteger("YAxis", 1);
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, normalSpeed);
            StartCoroutine(GetOutTimer());
        }
        objectToConsider = corner;
        count = 6;
        StartCoroutine(ScatterMode());
    }
    // Update is called once per frame
    void Update()
    {

        //Debug.Log(updateGhost, this.gameObject);
       // Debug.Break();
        if(updateGhost)
        {
            Vector2 currentDestination = new Vector2(CalculateNewXPosition(transform.position.x), CalculateNewYPosition(transform.position.y));
            if(enemyState == enemyStates.EnterPanic)
            {
                enemyState = enemyStates.Panic;
                DirectionUpdate(currentDestination, true, false);
            }
            if(enemyState == enemyStates.Panic)
            {
      
                if(!panicMode)
                {
                    enemyState = enemyStates.EnterChase;
                    DirectionUpdate(currentDestination, false, false);
                }
            }
            else if(enemyState == enemyStates.four)
            {
                enemyState = enemyStates.five;

                DirectionUpdate(currentDestination, false, true);
            }
            else if(enemyState == enemyStates.EnterChase)
            {
                if(panicMode)
                {
                    enemyState = enemyStates.Panic;

                    DirectionUpdate(currentDestination, true, false);
                }
            }
            else if((enemyState == enemyStates.PinkyStart || enemyState > enemyStates.six) && enemyState != enemyStates.nine)
            {
                if(panicMode)
                {
                    anim.SetBool("Panic", true);
                    anim.SetBool("Return", false);
                }
                else
                {
                    anim.SetBool("Panic", false);
                    anim.SetBool("Return", false);
                }
            }

            else if(enemyState == enemyStates.nine)
            {
                enemyState = enemyStates.EnterChase;

                DirectionUpdate(currentDestination, false, false);
            }
            if(nextDestination == currentDestination)
            {

                if(enemyState == enemyStates.EnterChase)
                {
                    DirectionUpdate(currentDestination, false, false);
                }
                else if(enemyState == enemyStates.PinkyStart)
                {
                    if(panicMode)
                    {
                        enemyState = enemyStates.Panic;

                        DirectionUpdate(currentDestination, true, false);

                    }
                    else
                    {
                        enemyState = enemyStates.EnterChase;
                        DirectionUpdate(currentDestination, false, false);
                    }
                }
                else if(enemyState == enemyStates.Panic)
                {
                    DirectionUpdate(currentDestination, true, false);
                }
                else if(enemyState == enemyStates.five)
                {
                    DirectionUpdate(currentDestination, false, true);
                }
                else if(enemyState == enemyStates.six)
                {
                    enemyState = enemyStates.PinkyStart;
                    GetComponent<Rigidbody2D>().velocity = new Vector2(0, getOutSpeed);
                    nextDestination = new Vector2(CalculateNewXPosition(afterExitDestination.x), CalculateNewYPosition(afterExitDestination.y));
                    anim.SetBool("Panic", false);
                    anim.SetBool("Return", false);
                    anim.SetInteger("XAxis", 0);
                    anim.SetInteger("YAxis", 1);
                }
            }
        }
    }

    IEnumerator GetOutTimer()
    {
        if(transform.position.x > 0)
        {
            yield return new WaitForSeconds(10f);
        }
        else
        {
            yield return new WaitForSeconds(3f);
        }
        if(enemyState != enemyStates.ten)
        {
            enemyState = enemyStates.eight;
            if(transform.position.x < 0)
            {
                transform.position = new Vector2(-1.05f, 0.7f);
                anim.SetInteger("XAxis", 1);
                anim.SetInteger("YAxis", 0);
                GetComponent<Rigidbody2D>().velocity = new Vector2(getOutSpeed, 0);
            }
            else if(transform.position.x > 0)
            {
                transform.position = new Vector2(1.05f, 0.7f);
                anim.SetInteger("XAxis", -1);
                anim.SetInteger("YAxis", 0);
                GetComponent<Rigidbody2D>().velocity = new Vector2(-getOutSpeed, 0);
            }
        }
    }

    public void DirectionUpdate(Vector2 currentDestination, bool runAway, bool returnToBase)
    {
        float speed = normalSpeed;
        if(runAway)
        {
            anim.SetBool("Panic", true);
            anim.SetBool("Return", false);
            speed = runAwaySpeed;
        }
        else if(returnToBase)
        {
            anim.SetBool("Return", true);
            anim.SetBool("Panic", false);
            speed = returnSpeed;
        }

        else if(!runAway && !returnToBase)
        {
            anim.SetBool("Panic", false);
            anim.SetBool("Return", false);
        }
        Vector2 direction = ChooseDirection(currentDestination, runAway, returnToBase);

        if(direction == Vector2.up)
        {
            transform.position = new Vector2(CalculateNewXPosition(transform.position.x), transform.position.y);
            anim.SetInteger("XAxis", 0);
            anim.SetInteger("YAxis", 1);
            //					anim.Play (blinkyUpAnimation);
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, speed);
        }
        else if(direction == -Vector2.up)
        {
            transform.position = new Vector2(CalculateNewXPosition(transform.position.x), transform.position.y);
            anim.SetInteger("XAxis", 0);
            anim.SetInteger("YAxis", -1);
            //					anim.Play (blinkyDownAnimation);
            GetComponent<Rigidbody2D>().velocity = new Vector2(0, -speed);
        }
        else if(direction == Vector2.right)
        {
            transform.position = new Vector2(transform.position.x, CalculateNewYPosition(transform.position.y));
            anim.SetInteger("XAxis", 1);
            anim.SetInteger("YAxis", 0);
            //					anim.Play (blinkyRightAnimation);
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
        }
        else if(direction == -Vector2.right)
        {
            transform.position = new Vector2(transform.position.x, CalculateNewYPosition(transform.position.y));
            anim.SetInteger("XAxis", -1);
            anim.SetInteger("YAxis", 0);
            //					anim.Play (blinkyLeftAnimation);
            GetComponent<Rigidbody2D>().velocity = new Vector2(-speed, 0);
        }
        previousDestination = new Vector2(CalculateNewXPosition(currentDestination.x), CalculateNewYPosition(currentDestination.y));
    }

    private Vector2 ChooseDirection(Vector2 currentDestination, bool runAway, bool returnToBase)
    {
        int x = CalculateNewXIndex(transform.position.x);
        int y = CalculateNewYIndex(transform.position.y);
        double goToUpScore = 1000000;
        double goToDownScore = 1000000;
        double goToRightScore = 1000000;
        double goToLeftScore = 1000000;

        double runAwayUpScore = -1000000;
        double runAwayDownScore = -1000000;
        double runAwayRightScore = -1000000;
        double runAwayLeftScore = -1000000;

        double lowestScore = 1000000;
        double highestScore = -1000000;

        Vector2 closestDestinationUp = currentDestination;
        Vector2 closestDestinationDown = currentDestination;
        Vector2 closestDestinationLeft = currentDestination;
        Vector2 closestDestinationRight = currentDestination;

        Vector2 direction = new Vector2(0, 0);

        if(valid(Vector2.up * 0.7f))
        {
            closestDestinationUp = FindCloseDestination(x, y, Vector2.up, currentDestination);
            if(closestDestinationUp != currentDestination)
            {
                if(!returnToBase && !runAway)
                {
                    goToUpScore = Vector2.Distance(closestDestinationUp, objectToConsider.transform.position);
                }
                else if(!returnToBase && runAway)
                {
                    goToUpScore = Vector2.Distance(closestDestinationUp, pacman.transform.position);
                }
                else
                {
                    goToUpScore = Vector2.Distance(closestDestinationUp, afterExitDestination);
                }
                runAwayUpScore = goToUpScore;
            }
        }
        if(valid(-Vector2.up * 0.7f))
        {
            closestDestinationDown = FindCloseDestination(x, y, -Vector2.up, currentDestination);
            if(closestDestinationDown != currentDestination)
            {
                if(!returnToBase && !runAway)
                {
                    goToDownScore = Vector2.Distance(closestDestinationDown, objectToConsider.transform.position);
                }
                else if(!returnToBase && runAway)
                {
                    goToDownScore = Vector2.Distance(closestDestinationDown, pacman.transform.position);
                }
                else
                {
                    goToDownScore = Vector2.Distance(closestDestinationDown, afterExitDestination);
                }
                runAwayDownScore = goToDownScore;
            }
        }
        if(valid(Vector2.right * 0.7f))
        {
            closestDestinationRight = FindCloseDestination(x, y, Vector2.right, currentDestination);
            if(closestDestinationRight != currentDestination)
            {
                if(!returnToBase && !runAway)
                {
                    goToRightScore = Vector2.Distance(closestDestinationRight, objectToConsider.transform.position);
                }

                else if(!returnToBase && runAway)
                {
                    goToRightScore = Vector2.Distance(closestDestinationRight, pacman.transform.position);
                }

                else
                {
                    goToRightScore = Vector2.Distance(closestDestinationRight, afterExitDestination);
                }
                runAwayRightScore = goToRightScore;
            }
        }
        if(valid(-Vector2.right * 0.7f))
        {
            closestDestinationLeft = FindCloseDestination(x, y, -Vector2.right, currentDestination);
            if(closestDestinationLeft != currentDestination)
            {
                if(!returnToBase && !runAway)
                {
                    goToLeftScore = Vector2.Distance(closestDestinationLeft, objectToConsider.transform.position);
                }

                else if(!returnToBase && runAway)
                {
                    goToLeftScore = Vector2.Distance(closestDestinationLeft, pacman.transform.position);
                }

                else
                {
                    goToLeftScore = Vector2.Distance(closestDestinationLeft, afterExitDestination);
                }
                runAwayLeftScore = goToLeftScore;
            }
        }
        direction = Vector2.up;
        nextDestination = closestDestinationUp;
        int bet = -1;
        if(!runAway)
        {
            lowestScore = goToUpScore;

            if(lowestScore >= goToDownScore)
            {
                if(goToDownScore == lowestScore)
                {
                    bet = random.Next() % 2;
                    if(bet == 1)
                    {
                        lowestScore = goToDownScore;
                        direction = -Vector2.up;
                        nextDestination = closestDestinationDown;
                    }
                }
                else
                {
                    lowestScore = goToDownScore;
                    direction = -Vector2.up;
                    nextDestination = closestDestinationDown;
                }
            }

            if(lowestScore >= goToRightScore)
            {
                if(goToRightScore == lowestScore)
                {
                    bet = random.Next() % 2;
                    if(bet == 1)
                    {
                        lowestScore = goToRightScore;
                        direction = Vector2.right;
                        nextDestination = closestDestinationRight;
                    }
                }
                else
                {
                    lowestScore = goToRightScore;
                    direction = Vector2.right;
                    nextDestination = closestDestinationRight;
                }
            }

            if(lowestScore >= goToLeftScore)
            {
                if(goToLeftScore == lowestScore)
                {
                    bet = random.Next() % 2;
                    if(bet == 1)
                    {
                        lowestScore = goToLeftScore;
                        direction = -Vector2.right;
                        nextDestination = closestDestinationLeft;
                    }
                }
                else
                {
                    lowestScore = goToLeftScore;
                    direction = -Vector2.right;
                    nextDestination = closestDestinationLeft;
                }
            }
        }

        else
        {
            highestScore = runAwayUpScore;

            if(highestScore <= runAwayDownScore)
            {
                if(runAwayDownScore == highestScore)
                {
                    bet = random.Next() % 2;
                    if(bet == 1)
                    {
                        highestScore = runAwayDownScore;
                        direction = -Vector2.up;
                        nextDestination = closestDestinationDown;
                    }
                }
                else
                {
                    highestScore = runAwayDownScore;
                    direction = -Vector2.up;
                    nextDestination = closestDestinationDown;
                }
            }

            if(highestScore <= runAwayRightScore)
            {
                if(runAwayRightScore == highestScore)
                {
                    bet = random.Next() % 2;
                    if(bet == 1)
                    {
                        highestScore = runAwayRightScore;
                        direction = Vector2.right;
                        nextDestination = closestDestinationRight;
                    }
                }
                else
                {
                    highestScore = runAwayRightScore;
                    direction = Vector2.right;
                    nextDestination = closestDestinationRight;
                }
            }

            if(highestScore <= runAwayLeftScore)
            {
                if(runAwayLeftScore == highestScore)
                {
                    bet = random.Next() % 2;
                    if(bet == 1)
                    {
                        highestScore = runAwayLeftScore;
                        direction = -Vector2.right;
                        nextDestination = closestDestinationLeft;
                    }
                }
                else
                {
                    highestScore = runAwayLeftScore;
                    direction = -Vector2.right;
                    nextDestination = closestDestinationLeft;
                }
            }
        }
        return direction;
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if(coll.name == "AfterExitTrigger")
        {
            if(enemyState == enemyStates.five)
            {
                anim.SetBool("Panic", false);
                anim.SetBool("Return", true);
                anim.SetInteger("XAxis", 0);
                anim.SetInteger("YAxis", -1);
                enemyState = enemyStates.six;
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, -normalSpeed);
                transform.position = new Vector2(0, CalculateNewYPosition(coll.transform.position.y));
            }
        }
        if(coll.name == "BeforeExitTrigger")
        {
            if(enemyState == enemyStates.six)
            {
                GameObject[] ghosts = GameObject.FindGameObjectsWithTag("Ghost");
                bool allGhostsReturned = true;
                foreach(GameObject aGhost in ghosts)
                {
                    GhostScript gScript = aGhost.GetComponent<GhostScript>();

                    if(gScript.enemyState == enemyStates.four || gScript.enemyState == enemyStates.five)
                    {
                        allGhostsReturned = false;
                        break;
                    }
                }
                if(allGhostsReturned)
                {
                    MainGameScript.ghostsReturning = false;
                }
            }
            if(enemyState == enemyStates.six || enemyState == enemyStates.eight)
            {
                enemyState = enemyStates.PinkyStart;
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, getOutSpeed);
                nextDestination = new Vector2(CalculateNewXPosition(afterExitDestination.x), CalculateNewYPosition(afterExitDestination.y));
                transform.position = new Vector2(0, CalculateNewYPosition(coll.transform.position.y));
                anim.SetBool("Panic", false);
                anim.SetBool("Return", false);
                anim.SetInteger("XAxis", 0);
                anim.SetInteger("YAxis", 1);
            }
        }

        if(coll.name == "GoDownTrigger")
        {
            if(enemyState == enemyStates.ClydeInkyStart)
            {
                anim.SetInteger("XAxis", 0);
                anim.SetInteger("YAxis", -1);
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, -normalSpeed);
            }
        }
        if(coll.name == "GoUpTrigger")
        {
            if(enemyState == enemyStates.ClydeInkyStart)
            {
                anim.SetInteger("XAxis", 0);
                anim.SetInteger("YAxis", 1);
                GetComponent<Rigidbody2D>().velocity = new Vector2(0, normalSpeed);
            }
        }

        else if(coll.tag == "Pacman")
        {
            if(enemyState == enemyStates.EnterChase)
            {
                StartCoroutine(WaitDeath());
            }
            if(enemyState == enemyStates.Panic)
            {
                StartCoroutine(EatenByPacman());
            }
        }
    }

    IEnumerator EatenByPacman()
    {
        GameObject[] ghosts = GameObject.FindGameObjectsWithTag("Ghost");

        foreach(GameObject aGhost in ghosts)
        {
            aGhost.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
        pacman.GetComponent<Renderer>().enabled = false;
        anim.enabled = false;
        PacmanScript pScript = pacman.GetComponent<PacmanScript>();
        //pScript.SavePreviousVelocity();
        // pScript.Status = 3;
        // MainGameScript.CalledEatenSound.Play();
        //if(mScript.EatGhostScore == 200)
        //{
        //    this.GetComponent<SpriteRenderer>().sprite = point200;

        //}
        //if(mScript.EatGhostScore == 400)
        //{
        //    this.GetComponent<SpriteRenderer>().sprite = point400;
        //}
        //if(mScript.EatGhostScore == 800)
        //{
        //    this.GetComponent<SpriteRenderer>().sprite = point800;
        //}
        //if(mScript.EatGhostScore == 1600)
        //{
        //    this.GetComponent<SpriteRenderer>().sprite = point1600;
        //}
        // scoreValue += mScript.EatGhostScore;
        // mScript.CheckNewLifeScore(mScript.EatGhostScore);
        // scoreValueGuiText.text = scoreValue.ToString();
        //  mScript.EatGhostScore = mScript.EatGhostScore * 2;
        yield return new WaitForSeconds(1f);
        MainGameScript.ghostsReturning = true;
        foreach(GameObject aGhost in ghosts)
        {
            if(aGhost != this.gameObject)
            {
                GhostScript gScript = aGhost.GetComponent<GhostScript>();
                Animator ghostAnimator = aGhost.GetComponent<Animator>();
                if(enemyState == enemyStates.Panic)
                {
                    enemyState = enemyStates.EnterPanic;
                }
                else if(gScript.enemyState == enemyStates.EnterChase)
                {
                    enemyState = enemyStates.nine;

                }
                else if(gScript.enemyState == enemyStates.eight)
                {

                    if(transform.position.x < 0)
                    {
                        aGhost.transform.position = new Vector2(-1.05f, 0.7f);

                        ghostAnimator.SetInteger("XAxis", 1);
                        ghostAnimator.SetInteger("YAxis", 0);
                        aGhost.GetComponent<Rigidbody2D>().velocity = new Vector2(getOutSpeed, 0);
                    }
                    else if(transform.position.x > 0)
                    {
                        aGhost.transform.position = new Vector2(1.05f, 0.7f);
                        ghostAnimator.SetInteger("XAxis", -1);
                        ghostAnimator.SetInteger("YAxis", 0);
                        aGhost.GetComponent<Rigidbody2D>().velocity = new Vector2(-getOutSpeed, 0);
                    }
                }
                else if(gScript.enemyState == enemyStates.PinkyStart)
                {
                    ghostAnimator.SetInteger("XAxis", 0);
                    ghostAnimator.SetInteger("YAxis", 1);
                    aGhost.GetComponent<Rigidbody2D>().velocity = new Vector2(0, getOutSpeed);
                }
                else if(gScript.enemyState == enemyStates.five)
                {
                    enemyState = enemyStates.four;
                }
                else if(gScript.enemyState == enemyStates.six)
                {
                    ghostAnimator.SetBool("Panic", false);
                    ghostAnimator.SetBool("Return", true);
                    ghostAnimator.SetInteger("XAxis", 0);
                    ghostAnimator.SetInteger("YAxis", -1);
                    aGhost.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -returnSpeed);
                    aGhost.transform.position = new Vector2(0, CalculateNewYPosition(aGhost.transform.position.y));
                }
            }
        }
        panicMode = false;
        pacman.GetComponent<Renderer>().enabled = true;
        GetComponent<Renderer>().enabled = true;
        anim.enabled = true;
        enemyState = enemyStates.four;


    }

    IEnumerator WaitDeath()
    {
        GameObject[] ghosts = GameObject.FindGameObjectsWithTag("Ghost");
        PacmanScript pScript = pacman.GetComponent<PacmanScript>();
        GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        foreach(GameObject aGhost in ghosts)
        {
            aGhost.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        }
        pacman.GetComponent<Rigidbody2D>().velocity = Vector2.zero;
        //pScript.UpdatePacman = false;
        GameObject mainGame = GameObject.Find("MainGame");
        MainGameScript mScript = mainGame.GetComponent<MainGameScript>();
        mScript.status = 3;
        mScript.GetComponent<AudioSource>().enabled = false;
        //  mScript.panicSound.enabled = false;
        //  mScript.returnSound.enabled = false;
        //  mScript.hurryUpSound.enabled = false;
        //    mScript.StopAllModes();
        MainGameScript.ghostsReturning = false;
        yield return new WaitForSeconds(0.5f);
        GameObject[] fruits = GameObject.FindGameObjectsWithTag("Fruit");
        if(fruits.Length > 0)
        {
            Destroy(fruits[0]);
        }
        // pScript.UpdatePacman = true;
        // pScript.Status = 2;
        foreach(GameObject aGhost in ghosts)
        {
            Destroy(aGhost);
        }

    }

    bool valid(Vector2 direction)
    {
        Vector2 ghostPosition = transform.position;
        RaycastHit2D hit = Physics2D.Linecast(ghostPosition + direction, ghostPosition);
        if(hit.collider.isTrigger)
            return true;
        return (hit.collider == GetComponent<Collider2D>());
    }

    private Vector2 FindCloseDestination(int x, int y, Vector2 direction, Vector2 currentDestination)
    {
        Vector2 closestDestination = currentDestination;
        Vector2 newDestination;
        if(direction == -Vector2.up)
        {
            for(int currentIndex = y + 1; currentIndex < junctions.GetLength(0); currentIndex++)
            {
                if(junctions[currentIndex, x] == 1)
                {
                    newDestination = new Vector2(CalculateNewXPosition(x), CalculateNewYPosition(currentIndex));
                    if(previousDestination != newDestination)
                    {
                        closestDestination = newDestination;
                    }
                    break;
                }
            }
        }
        else if(direction == Vector2.up)
        {
            for(int currentIndex = y - 1; currentIndex >= 0; currentIndex--)
            {
                if(junctions[currentIndex, x] == 1)
                {
                    newDestination = new Vector2(CalculateNewXPosition(x), CalculateNewYPosition(currentIndex));
                    if(previousDestination != newDestination)
                    {
                        closestDestination = newDestination;
                    }
                    break;
                }
            }
        }
        else if(direction == Vector2.right)
        {
            if(x == junctions.GetLength(1) - 1)
            {
                newDestination = new Vector2(CalculateNewXPosition(0), CalculateNewYPosition(y));
                if(previousDestination != newDestination)
                {
                    closestDestination = newDestination;
                }
            }
            for(int currentIndex = x + 1; currentIndex < junctions.GetLength(1); currentIndex++)
            {
                if(junctions[y, currentIndex] == 1)
                {
                    newDestination = new Vector2(CalculateNewXPosition(currentIndex), CalculateNewYPosition(y));
                    if(previousDestination != newDestination)
                    {
                        closestDestination = newDestination;
                    }
                    break;
                }
            }
        }
        else if(direction == -Vector2.right)
        {
            if(x == 0)
            {
                newDestination = new Vector2(CalculateNewXPosition(junctions.GetLength(1) - 1), CalculateNewYPosition(y));
                if(previousDestination != newDestination)
                {
                    closestDestination = newDestination;
                }
            }
            for(int currentIndex = x - 1; currentIndex >= 0; currentIndex--)
            {
                if(junctions[y, currentIndex] == 1)
                {
                    newDestination = new Vector2(CalculateNewXPosition(currentIndex), CalculateNewYPosition(y));
                    if(previousDestination != newDestination)
                    {
                        closestDestination = newDestination;
                    }
                    break;
                }
            }
        }

        return closestDestination;
    }

    private float CalculateNewXPosition(float xTransform)
    {
        int mazeIndex;

        float xFloat = (9.45f + xTransform) / 0.7f;
        mazeIndex = (int)System.Math.Round(xFloat, 0);
        float realXPosition = (-9.45f) + (mazeIndex * 0.7f);
        return realXPosition;
    }

    private float CalculateNewYPosition(float yTransform)
    {
        int mazeIndex;
        float yFloat = (10.5f - yTransform) / 0.7f;
        mazeIndex = (int)System.Math.Round(yFloat, 0);
        float realYPosition = 10.5f - (mazeIndex * 0.7f);
        return realYPosition;
    }

    private float CalculateNewXPosition(int mazeIndex)
    {
        float realXPosition = (-9.45f) + (mazeIndex * 0.7f);
        return realXPosition;
    }

    private float CalculateNewYPosition(int mazeIndex)
    {
        float realYPosition = 10.5f - (mazeIndex * 0.7f);
        return realYPosition;
    }

    private int CalculateNewXIndex(float xTransform)
    {
        int mazeIndex;
        float xFloat = (9.45f + xTransform) / 0.7f;
        mazeIndex = (int)System.Math.Round(xFloat, 0);
        return mazeIndex;
    }

    private int CalculateNewYIndex(float yTransform)
    {
        int mazeIndex;
        float yFloat = (10.5f - yTransform) / 0.7f;
        mazeIndex = (int)System.Math.Round(yFloat, 0);
        return mazeIndex;
    }





}

public enum enemyStates
{
    EnterChase,//0
    PinkyStart,//1
    Panic,//2
    EnterPanic,//3
    four,// panic
    five,// panic
    six,//  panic
    ClydeInkyStart,
    eight,
    nine,
    ten,
}
