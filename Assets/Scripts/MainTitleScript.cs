﻿using UnityEngine;
using System.Collections;

public class MainTitleScript : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
		if (Input.GetKeyDown (KeyCode.Space)) 
		{
			StartCoroutine(StartGame());
		}
		if (Input.GetKeyDown (KeyCode.Escape)) {
			Application.Quit ();
		}
	}
	IEnumerator StartGame()
	{
		GetComponent<AudioSource>().Play();
		PlayerPrefs.SetInt ("Level", 1);
		PlayerPrefs.SetInt ("Score", 0);
		PlayerPrefs.SetInt ("Lives", 3);
		PlayerPrefs.SetInt ("ScoreUntilNextLife", 0);
		yield return new WaitForSeconds (0.2f);
		Application.LoadLevel(1);
	}
}
