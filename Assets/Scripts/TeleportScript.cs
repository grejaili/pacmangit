﻿using UnityEngine;
using System.Collections;

public class TeleportScript : MonoBehaviour {
	
	void OnTriggerEnter2D(Collider2D coll) 
	{
		if (coll.tag == "Pacman" || coll.tag == "Ghost") 
		{
			if(this.name == "RightTeleport")
			{
				coll.transform.position = new Vector2(-9.45f,0.7f);
			}
			else if(this.name == "LeftTeleport")
			{
				coll.transform.position = new Vector2(9.45f,0.7f);
			}
		}
	}
}
